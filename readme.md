<h1 align="center"><u>Laravel KULWAP 1</u></h1>

<p align="center">Kulwap Laravel BUILD App With Auth ACL and Datatables</p>


<p align="center">
<a href="https://ibb.co/ci3d3F"><img src="https://preview.ibb.co/c6BWOF/HASIL.png" alt="HASIL" border="0"></a>
</p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


# Installation



## Server Requirements

Kerangka kerja Laravel memiliki beberapa persyaratan sistem. Tentu saja, semua persyaratan ini dipenuhi oleh mesin virtual Laravel Homestead, jadi sangat disarankan agar Anda menggunakan Homestead sebagai lingkungan pengembangan Laravel setempat.

Namun, jika Anda tidak menggunakan Homestead, Anda harus memastikan server Anda memenuhi persyaratan berikut:
1. PHP >= 5.6.4
2. OpenSSL PHP Extension
3. PDO PHP Extension
4. Mbstring PHP Extension
5. Tokenizer PHP Extension
6. XML PHP Extension


## Installasi 

Clone terlebih dahulu dengan perintah di bawah

```bash
git clone https://virusphp@gitlab.com/kulwap/kulwap_1.git
```

## License

Laravel Markdown is licensed under [The MIT License (MIT)](LICENSE).
