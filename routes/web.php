<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function() {
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::auth();
	Route::get('/home', 'HomeController@index');
	Route::get('contact', 'ContactController@index')->middleware('auth');
	
	Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function() {
		Route::get('contact/create' , 'ContactController@create');
		Route::post('contact/store' , 'ContactController@store');
		Route::get('contact/edit/{edit}', 'ContactController@edit');
		Route::put('contact/update/{update}', 'ContactController@update');
		Route::delete('contact/delete/{del}', 'ContactController@destroy');
	});
});