@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/contact') }}">contact</a></li>
					<li class="active">Edit contact</a></li>
				</ul>
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Edit contact</h2>
					</div>
					<div class="panel-body">
						<!-- <form action="{{ url('admin/contact/store') }}" method="post" class="form-horizontal">
							{{ csrf_field() }}
							@include('contact._form')
						</form> -->

						{!! Form::model($contact, ['method' => 'PUT', 'url' => ['admin/contact/update', $contact->id], 'class' => 'form-horizontal']) !!}
							@include('contact._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection('content')