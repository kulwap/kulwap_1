	<div class="form-group" {{ $errors->has('name') ? 'has-errors' : '' }}>
		<label for="name" class="col-md-2 control-label">Name</label>
		<div class="col-md-4">
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
			{!! $errors->first('name', '<p class="help-blcok">:message</p>') !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('alamat') ? 'has-errors' : '' }}>
		<label for="alamat" class="col-md-2 control-label">Alamat</label>
		<div class="col-md-4">
			{!! Form::text('alamat', null, ['class' => 'form-control']) !!}
			{!! $errors->first('alamat', '<p class="help-blcok">:message</p>') !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('pekerjaan') ? 'has-errors' : '' }}>
		<label for="pekerjaan" class="col-md-2 control-label">Pekerjaan</label>
		<div class="col-md-4">
			{!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
			{!! $errors->first('pekerjaan', '<p class="help-blcok">:message</p>') !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('no_telp') ? 'has-errors' : '' }}>
		<label for="no_telp" class="col-md-2 control-label">No Telepon</label>
		<div class="col-md-4">
			{!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
			{!! $errors->first('no_telp', '<p class="help-blcok">:message</p>') !!}
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-4 col-md-offset-2">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>